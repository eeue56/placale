import scala.annotation.tailrec

class Node (val value : Int){

	var left : Node = null
	var right : Node = null


	@tailrec final def insert(node : Node) : Unit = {
		if (node.value < value){ 
			if (left == null) left = node else left.insert(node)
		}
		else if (node.value > value){
			if (right == null) right = node else right.insert(node)
		}
	}

	@tailrec final def deepestLeft() : Node = {
		if (left == null) this else left.deepestLeft()
	}

	@tailrec final def deepestRight() : Node = {
		if (right == null) this else right.deepestRight()
	}

	@tailrec final def search(value : Int) : Node = {
		if (this.value == value){
			this
		}
		else if (this.value < value) {
			if (left == null){
				null
			}
			else {
				left.search(value)
			}
		}
		else if {this.value > value){
			if (right == null){
				null
			}
			else{
				right.search(value)
			}
		}
		else{
			null
		}
	}

	def children() : Int = {
		@tailrec def leftHelper(node : Node, i : Int) : Int = {
			if (node.left == null) i else leftHelper(node.left, i + 1)
		}

		@tailrec def rightHelper(node : Node, i : Int) : Int = {
			if (node.right == null) i else rightHelper(node.right, i + 1)
		}

		leftHelper(this, 0) + rightHelper(this, 0)
	}

	override def toString() : String = {
		value + ": children=" + this.children()
	}
}

var n = new Node(4)
n.insert(new Node(5))

var i = 0

while (i < 10){
	n.insert(new Node(i))
	i = i + 1
}

println("Root: " + n)
println("Deepest left: " + n.deepestLeft())
println("Deepest right: " + n.deepestRight())
println("Contains 2: " + n.search(2))
println("Contains 100: " + n.search(100))